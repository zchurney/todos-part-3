// For the next part of part 2 we want our URL + /active, + /completed, etc. We basically
// want a couple different URLs that show our app in various states/stages

import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList.js";
import { Route, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { addTodo, clearCompletedTodos } from "./actions";

class App extends Component {
  // this.state.todos - App component state
  // this.props.todos - redux state
  state = {
    todos: todosList
  };

  handleCreateTodo = event => {
    if (event.key === "Enter") {
      this.props.addTodo(event.target.value);
      event.target.value = "";
    }
  };

  clearCompletedTodos = event => {
    console.log("todos were cleared");
    this.props.clearCompletedTodos();
  };

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1> todos </h1>{" "}
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            onKeyDown={this.handleCreateTodo}
          />{" "}
        </header>{" "}
        <Route exact path="/">
          <TodoList todos={this.props.todos} />{" "}
        </Route>{" "}
        <Route exact path="/active">
          <TodoList
            todos={this.props.todos.filter(todo => todo.completed === false)}
          />{" "}
        </Route>{" "}
        <Route exact path="/completed">
          <TodoList
            todos={this.props.todos.filter(todo => todo.completed === true)}
          />{" "}
        </Route>{" "}
        <footer className="footer">
          {" "}
          {/* <!-- This should be `0 items left` by default --> */}{" "}
          <span className="todo-count">
            <strong>
              {" "}
              {
                this.state.todos.filter(todo => {
                  return todo.completed !== true;
                }).length
              }{" "}
            </strong>{" "}
            item(s) left{" "}
          </span>{" "}
          <ul className="filters">
            <li>
              <NavLink exact activeClassName="selected" to="/">
                All{" "}
              </NavLink>{" "}
            </li>{" "}
            <li>
              <NavLink activeClassName="selected" to="/active">
                Active{" "}
              </NavLink>{" "}
            </li>{" "}
            <li>
              <NavLink activeClassName="selected" to="/completed">
                Completed{" "}
              </NavLink>{" "}
            </li>{" "}
          </ul>{" "}
          <button
            className="clear-completed"
            onClick={this.clearCompletedTodos}
          >
            Clear completed{" "}
          </button>{" "}
        </footer>{" "}
      </section>
    );
  }
}

//asking connect to read certain values from the redux state
const mapStateToProps = state => {
  return {
    todos: state.todos // array of todo objects
  };
};

// this.props.addTodo
const mapDispatchToProps = {
  addTodo,
  clearCompletedTodos
};

// connect allows you to get state values from the redux state and make them props on your component
// automatically causes your component to re-render when those state values change
// connect is doing this: store.subscribe(() => {})
// connect allows you to get action creator functions and make them props on your component
// The other thing connect does is automatically call store.dispatch() and passes in the addTodoAction
export default connect(mapStateToProps, mapDispatchToProps)(App);
